from django import forms
from django.forms import ModelForm
from django.forms import fields
from homepage.models import Harapan


forms
class FormHarapan(ModelForm):
    class Meta:
        model = Harapan
        fields = "__all__"

        widgets = {
            'nama' : forms.TextInput({'class' : 'form-control', 'placeholder':'Pemimpi'}),
            'isi' : forms.Textarea({'class' : 'form-control','maxlength':'100', 'placeholder':'Saya berharap kedepannya...'}),
        }
        