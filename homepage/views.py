from django.http.response import HttpResponseRedirect
from django.shortcuts import render
from homepage.forms import FormHarapan
from homepage.models import Harapan

# Create your views here.
def home(request):
    if request.POST:
        form = FormHarapan(request.POST)
        if form.is_valid:
            form.save()
            form = FormHarapan()
            return HttpResponseRedirect('/')
    else:
        form = FormHarapan()
        harapans = Harapan.objects.all()
        context = {
            'harapans':harapans,
            'form':form
        }
        return render(request, 'home.html', context)