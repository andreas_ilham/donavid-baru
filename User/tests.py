from django.test import TestCase, Client
from .models import UserProfile
from django.contrib.auth.models import User

# Create your tests here.
class PemberianDonasiTest(TestCase):

    

    def test_register_url(self):
        response = self.c.get('/user/register')
        self.assertEqual(response.status_code, 200)

    def test_redirect_logout(self):
        response = self.c.get('/user/logout')
        self.assertEqual(response.status_code, 302)
