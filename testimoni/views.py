from django.http.response import HttpResponseRedirect
from django.shortcuts import render
from testimoni.forms import FormTestimoni
from testimoni.models import Testimoni

# Create your views here.
def testimoni(request):
    if request.POST:
        form = FormTestimoni(request.POST)
        if form.is_valid:
            form.save()
            form = FormTestimoni()
            return HttpResponseRedirect('/testimoni/')
    else:
        form = FormTestimoni()
        testimonis = Testimoni.objects.all()
        context = {
            'testimonis':testimonis,
            'form':form
        }
        return render(request, 'testimoni.html', context)
    