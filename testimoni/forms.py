from django import forms
from django.forms import ModelForm
from django.forms import fields
from testimoni.models import Testimoni


#forms
class FormTestimoni(ModelForm):
    class Meta:
        model = Testimoni
        fields = "__all__"

        widgets = {
            'nama' : forms.TextInput({'class' : 'form-control', 'placeholder':'Mang Oleh'}),
            'isi' : forms.Textarea({'class' : 'form-control','maxlength':'100', 'placeholder':'Donavid menurut saya...'}),
        }
        