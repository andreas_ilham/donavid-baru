
from django.db import models

# Create your models here.
class Testimoni(models.Model):
    nama = models.CharField(max_length=15)
    isi = models.TextField(max_length=150)

    def __str__(self):
        return self.nama