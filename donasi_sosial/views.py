from django.shortcuts import render, redirect
from .models import Program

# Create your views here.

def list_program(request):
    programs = Program.objects.all()
    response = {'programs': programs}
    return render(request, 'donasi.html', response)


def list_donatur(request, program):
    donasi_terkumpul = Program.objects.get(pk=program)
    nama_program = Program.objects.get(pk=program).name
    response = {
        'donasi_terkumpul': donasi_terkumpul,
        'nama_program' : nama_program,
        }
    return render(request, 'list_donatur.html', response)

