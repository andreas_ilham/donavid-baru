from django.db import models

# Create your models here.
class DonasiAPD(models.Model):
    nama = models.CharField(max_length=200, blank=True)
    email = models.EmailField(max_length=100)
    jmldonasi = models.IntegerField()
    is_anon = models.BooleanField()