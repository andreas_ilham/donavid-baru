from django.db import models
from formulir_donasi_apd.models import DonasiAPD

# Create your models here.
class Donasiapd(models.Model):
    status = models.CharField(max_length=100)
    name   = models.CharField(max_length=200)
    photo  = models.ImageField(upload_to="static/img")
    desc   = models.TextField()
    target = models.IntegerField()
    remDay = models.IntegerField()
    donatur = models.ManyToManyField(DonasiAPD, blank=True)
