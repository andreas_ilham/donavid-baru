# Generated by Django 3.1.2 on 2020-11-15 11:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('formulir_donasi_apd', '0001_initial'),
        ('donasi_apd', '0004_auto_20201115_1050'),
    ]

    operations = [
        migrations.AlterField(
            model_name='donasiapd',
            name='donatur',
            field=models.ManyToManyField(blank=True, to='formulir_donasi_apd.DonasiAPD'),
        ),
    ]
