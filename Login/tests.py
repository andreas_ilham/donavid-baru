from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest

import unittest

# Create your tests here.
# Unit Tests
class UnitTest(TestCase):
    def test_url_slash_login_slash_logout_slash_is_exist(self):
        response = Client().get('/login/logout/')
        self.assertEqual(response.status_code, 302)
